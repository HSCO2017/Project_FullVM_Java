/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Harry
 */
public class CodeWriter {
    private static BufferedWriter writerFile;
    private int arthJumpFlag;
    private int addCount;
    private static String fileName = "";
    private static final String arithmetic =
            "@SP\n" +
            "AM=M-1\n" +
            "D=M\n" +
            "A=A-1\n";
       
    public CodeWriter(File outputFile) throws IOException{
        fileName = outputFile.getName();
        FileWriter w = new FileWriter(outputFile);
        writerFile = new BufferedWriter(w);
    }
    
    public void writeArithmetic(String command) throws IOException{
        switch(command){
            case "add":
                writerFile.write(arithmetic + "M=M+D\n");
                break;
            case "sub":
                writerFile.write(arithmetic + "M=M-D\n");
                break;
            case "neg":
                writerFile.write("D=0\n"
                        + "@SP\n"
                        + "A=M-1\n"
                        + "M=D-M\n");
                break;
            case "eq":
                writerFile.write(arithmethicJump("JNE"));
                arthJumpFlag++;
                break;
            case "gt":
                writerFile.write(arithmethicJump("JLE"));
                arthJumpFlag++;
                break;
            case "lt":
                writerFile.write(arithmethicJump("JGE"));
                arthJumpFlag++;
                break;
            case "and":
                writerFile.write(arithmetic + "M=M&D\n");
                break;
            case "or":
                writerFile.write(arithmetic + "M=M|D\n");
                break;
            case "not":
                writerFile.write("@SP\n"
                        + "A=M-1\n"
                        + "M=!M\n");
                break;
        }
    }
    
    public void writePushPop(String command, String segment, int index) throws IOException{
        if (command.equals("C_PUSH")) {
            if (segment.equals("constant")){
                writerFile.write("@" + index + "\n"
                        + "D=A\n"
                        + "@SP\n"
                        + "A=M\n"
                        + "M=D\n"
                        + "@SP\n"
                        + "M=M+1\n");
            }
            else if (segment.equals("local")){
                writerFile.write(push("LCL",index,false));
            }
            else if (segment.equals("argument")){
                writerFile.write(push("ARG",index,false));
            }
            else if (segment.equals("this")){
                writerFile.write(push("THIS",index,false));
            }
            else if (segment.equals("that")){
                writerFile.write(push("THAT",index,false));
            }
            else if (segment.equals("temp")){
                writerFile.write("@R5\n" 
                        + "D=A\n"
                        + "@" + index + "\n"
                        + "A=D+A\n"
                        + "D=M\n"
                        + "@SP\n" 
                        + "A=M\n" 
                        + "M=D\n" 
                        + "@SP\n" 
                        + "M=M+1\n");
            }
            else if (segment.equals("pointer") && index == 0){
                writerFile.write(push("THIS",index,true));                
            }
            else if (segment.equals("pointer") && index == 1){
                writerFile.write(push("THAT",index,true));
            }
            else if (segment.equals("static")){
                writerFile.write("@" + fileName + index + "\n" 
                        + "D=M\n"
                        + "@SP\n"
                        + "A=M\n"
                        + "M=D\n"
                        + "@SP\n"
                        + "M=M+1\n");
            }
        }
        else if (command.equals("C_POP")) {
            if (segment.equals("local")){
                writerFile.write(pop("LCL",index,false));
            }
            else if (segment.equals("argument")){
                writerFile.write(pop("ARG",index,false));
            }
            else if (segment.equals("this")){
                writerFile.write(pop("THIS",index,false));
            }
            else if (segment.equals("that")){
                writerFile.write(pop("THAT",index,false));
            }
            else if (segment.equals("temp")){
                writerFile.write("@R5\n" 
                        + "D=A\n"
                        + "@" + index + "\n"
                        + "D=D+A\n"
                        + "@R13\n" 
                        + "M=D\n" 
                        + "@SP\n" 
                        + "AM=M-1\n" 
                        + "D=M\n" 
                        + "@R13\n" 
                        + "A=M\n" 
                        + "M=D\n");
            }
            else if (segment.equals("pointer") && index == 0){
                writerFile.write(pop("THIS",index,true));
            }
            else if (segment.equals("pointer") && index == 1){
                writerFile.write(pop("THAT",index,true));
            }
            else if (segment.equals("static")){
                writerFile.write(
                        "@" + fileName + index + "\n"
                        + "D=A\n"
                        + "@R13\n"
                        + "M=D\n"
                        + "@SP\n"
                        + "AM=M-1\n"
                        + "D=M\n"
                        + "@R13\n"
                        + "A=M\n"
                        + "M=D\n"
                );
            }
        }
    }
    
    /**
     *
     * @throws IOException
     */
    public void close() throws IOException{
        writerFile.flush();
        writerFile.close();
    }
    
    private String arithmethicJump(String type){
        return "@SP\n" +
                "AM=M-1\n" +
                "D=M\n" +
                "A=A-1\n" +
                "D=M-D\n" +
                "@FALSE" + arthJumpFlag + "\n" +
                "D;" + type + "\n" +
                "@SP\n" +
                "A=M-1\n" +
                "M=-1\n" +
                "@CONTINUE" + arthJumpFlag + "\n" +
                "0;JMP\n" +
                "(FALSE" + arthJumpFlag + ")\n" +
                "@SP\n" +
                "A=M-1\n" +
                "M=0\n" +
                "(CONTINUE" + arthJumpFlag + ")\n";
    }

    private String push(String segment, int index, boolean direct) {
        String noPointer = "";
        if (!direct) {
            noPointer = "@" + index + "\n" 
                    + "A=D+A\n"
                    + "D=M\n";
        }
        return "@" + segment + "\n" 
                + "D=M\n"
                + noPointer 
                + "@SP\n" 
                + "A=M\n" 
                + "M=D\n" 
                + "@SP\n" 
                + "M=M+1\n";
    }
    
    private String pop(String segment, int index, boolean direct) {
        String noPointer = "";
        if (!direct) {
            /*noPointer = "@" + index + "\n" 
                    + "A=D+A\n"
                    + "D=M\n";*/
            noPointer = "D=M\n"
                    + "@" + index + "\n" 
                    + "D=D+A\n";
        }
        else{
            noPointer = "D=A\n";
        }
        return "@" + segment + "\n" 
                + noPointer 
                + "@R13\n" 
                + "M=D\n" 
                + "@SP\n" 
                + "AM=M-1\n" 
                + "D=M\n" 
                + "@R13\n" 
                + "A=M\n" 
                + "M=D\n";
    }
    
    public void writeInit() throws IOException{
        writerFile.write("@256\n" 
                + "D=A\n"
                + "@SP\n" 
                + "M=D\n");
        writeCall("Sys.init", 0);
    }
    
    public void writeLabel(String label) throws IOException{
        writerFile.write("(" + label + ")\n");
    }
    
    public void writeGoto(String label) throws IOException{
        writerFile.write("@" + label +"\n"
                + "0;JMP\n");
    }
    
    public void writeIf(String label) throws IOException{
        writerFile.write("@SP\n" 
                + "AM=M-1\n" 
                + "D=M\n" 
                + "A=A-1\n"
                + "@" + label + "\n"
                + "D;JNE\n");
    }
    
    public void writeCall(String functionName, int numArgs) throws IOException{
        String label = "RETURN_ADD" + (addCount++);
        
        writerFile.write("@" + label + "\n" 
                + "D=A\n"
                + "@SP\n"
                + "A=M\n"
                + "M=D\n"
                + "@SP\n"
                + "M=M+1\n");
        writerFile.write(push("LCL", 0, true));
        writerFile.write(push("ARG", 0, true));
        writerFile.write(push("THIS", 0, true));
        writerFile.write(push("THAT", 0, true));
        writerFile.write("@SP\n" 
                + "D=M\n" 
                + "@5\n" 
                + "D=D-A\n" 
                + "@" + numArgs + "\n" 
                + "D=D-A\n" 
                + "@ARG\n" 
                + "M=D\n" 
                + "@SP\n" 
                + "D=M\n" 
                + "@LCL\n" 
                + "M=D\n" );
                writeGoto(functionName); 
                writeLabel(label);
    }
    
    public void writeReturn() throws IOException{
        writerFile.write("@LCL\n" 
                + "D=M\n" 
                + "@FRAME\n" 
                + "M=D\n" 
                + "@5\n" 
                + "A=D-A\n" 
                + "D=M\n" 
                + "@RET\n" 
                + "M=D\n" 
                + pop("ARG",0,false) 
                + "@ARG\n" 
                + "D=M\n" 
                + "@SP\n" 
                + "M=D+1\n" 
                + frame("THAT") 
                + frame("THIS") 
                + frame("ARG") 
                + frame("LCL") 
                + "@RET\n" 
                + "A=M\n" 
                + "0;JMP\n");
    }
    
    public String frame(String type){
        return "@FRAME\n" 
                + "D=M-1\n" 
                + "AM=D\n"
                + "D=M\n"
                + "@" + type + "\n" 
                + "M=D\n";
    }
    
    public void writeFunction(String functionName, int numLocals) throws IOException{
        writerFile.write("(" + functionName +")\n");
        for (int i = 0; i < numLocals; i++) {
            writePushPop("C_PUSH", "constant", 0);
        }
    }
    
    public void setFileName(File file){
        fileName = file.getName();
    }
}
