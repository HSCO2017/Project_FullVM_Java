/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Harry
 */
public class VM {

    public static ArrayList<File> vmFiles;
    public static CodeWriter writer;
    
    public static void main(String[] args) {
        // TODO code application logic here
        try {            
            if (false/*args.length != 1*/){
            System.out.println("Usage:java VM [filename|directory]");
            }
            else {
//                File inputFile = new File(args[0]); 
                Scanner reader = new Scanner(System.in);
                boolean option = true;
                boolean initb = false;
                while (option) {                    
                    System.out.println("Sys.init is necessary: Y/N");
                    String init = reader.next(); 
                    if (init.equals("Y")||init.equals("y")||init.equals("N")||init.equals("n")) {
                        option = false;
                        if (init.equals("Y")||init.equals("y")){
                            initb = true;
                        }
                        else{
                            initb = false;
                        }
                    }
                    else{
                        System.out.println("Input should be Y or N.");
                    }
                }                
                System.out.println("Input file address:");                
                String address = reader.next();
                File inputFile = new File(address); 
                String outFile = "";
                boolean flag = false;
                if (inputFile.isFile()) {
                    if (fileExtension(inputFile)) {
                        vmFiles.add(inputFile);
                        flag = true;
                        outFile = inputFile.getAbsolutePath();
                        outFile = outFile.replace(".vm", ".asm");
                    }
                    else{
                        System.out.println("The file extension is not .vm.");
                    }
                }
                else if (inputFile.isDirectory()) {
                    vmFiles = getFiles(inputFile);
                    if (vmFiles.isEmpty()) {
                        System.out.println("There are no .vm files in this directory.");
                    }
                    outFile = inputFile.getAbsolutePath() + "/" + inputFile.getName() + ".asm";
                }
                
                File outputFile = new File(outFile);
                outputFile.createNewFile(); 
                writer = new CodeWriter(outputFile);
                
                if (initb) {
                    writer.writeInit();
                }               
                
                for (int i = 0; i < vmFiles.size(); i++) {
                    Parse(vmFiles.get(i));
                }
                
                writer.close();
                
                if (flag) {
                    System.out.println("The file has been converted.");
                }
                else{
                    System.out.println("The files have been converted.");
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }

    public VM() {
        VM.vmFiles = new ArrayList<>();
    }
    
    private static boolean fileExtension(File inputF){
        String outFileName = inputF.getName();
        int indexExtension = outFileName.lastIndexOf(".");
        String outFileNameE = outFileName.substring(indexExtension);
        return outFileNameE.equals(".vm");
    }
    
    public static ArrayList<File> getFiles(File directory){
        File[] files = directory.listFiles();
        ArrayList<File> vmF = new ArrayList<>();
        for (File file : files) {
            if (file.getName().endsWith(".vm")) {
                vmF.add(file);
            }
        }
        return vmF;
    }
    
    public static void Parse(File input) throws IOException{
        
        writer.setFileName(input);
        Parser parser = new Parser(input);
        
        while (parser.hasMoreCommands()) {            
            parser.advance();
            String type = parser.commandType();
            switch(type){
                case "C_ARITHMETIC":
                    writer.writeArithmetic(parser.arg1());
                    break;
                case "C_POP":
                case "C_PUSH":
                    writer.writePushPop(type, parser.arg1(), parser.arg2());
                    break;
                case "C_LABEL":
                    writer.writeLabel(parser.arg1());
                    break;
                case "C_GOTO":
                    writer.writeGoto(parser.arg1());
                    break;
                case "C_IF":
                    writer.writeIf(parser.arg1());
                    break;
                case "C_FUNCTION":
                    writer.writeFunction(parser.arg1(), parser.arg2());
                    break;
                case "C_RETURN":
                    writer.writeReturn();
                    break;
                case "C_CALL":
                    writer.writeCall(parser.arg1(), parser.arg2());
                    break;
            }
            
        }
    }
}
