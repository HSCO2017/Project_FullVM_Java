Instrucciones para correr el programa:

-Abrir la consola.
-Escribir el comando "java -jar Assembler.jar" esto si se encuentra ubicado en la direcci�n del archivo, de lo contrario adjuntar la  direcci�n junto con el nombre del archivo.
-El programa fue trabajado en consola, inicialmente pregunta si el archivo a convertir necesita que se agregue el bootstrap (Sys.init) y luego le solicita la direcci�n del archivo.

-Si la conversi�n fue exitosa se creara el archivo en la misma direcci�n del archivo original, con el mismo nombre, ya sea del archivo o de la carpeta pero con la extension  .asm.

NOTA: Se debe de tener cuidado, si en la direcci�n se encuentra un espacio la libreria "File" de Java no la acepta y devolvera un error.
El proyecto fue originalmente trabajado bajo este repositorio, en este se encuentran los diferentes commits.
https://gitlab.com/HSCO2017/Project_FullVM_Java